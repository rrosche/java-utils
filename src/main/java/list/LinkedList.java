package list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T extends Comparable<? super T>> implements Iterable<T>{

	Node first;
	Node last;
	int size;
	
	protected class Node implements Comparable<Node>{
		T data;
		Node next;

		Node(T data){
			this.data = data;
		}

		@Override
		public int compareTo(Node o) {
			return data.compareTo(o.data);
		}
		
		
	}
	
	public LinkedList(){}
	
	protected LinkedList(Node head){
		this.first = head;
		size++;
	}

	public void insert(T item){
		Node node = new Node(item);
		
		if(first == null){
			first = node;
		}
			
		if(last != null){
			last.next = node;
		}
		
		last = node;
		size++;
	}

	public boolean find(T item){
		return findHelper(first,item);
	}
	
	public T remove(T item) {
		if(size == 0){
			return null;
		}
		if(first.data.equals(item)){
			T removed = first.data;
			if(first==last){
				last = first.next;
			}
			first = first.next;
			size--;
			return removed;
		}
		return removeHelper(first, item);
	}

	public int size(){
		return size;
	}
	
	public T getFirst(){
		if(first == null){
			return null;
		}else{
			return first.data;
		}
	}
	
	public T getLast(){
		if(last ==  null){
			if(size > 0){
				LinkedList<T>.Node temp = first;
				while(temp.next != null){
					temp = temp.next;
				}
				last = temp;
				return last.data;
			}else{
				return null;
			}
		}else{
			return last.data;
		}
	}
	
	private boolean findHelper(Node current, T item){
		if(current == null){
			return false;
		}
		
		if(current.data.equals(item)){
			return true;
		}

		return findHelper(current.next,item);
	}
	
	private T removeHelper(Node current, T item){
		if(current.next.data.equals(item)){
			T removed = current.next.data;
			if(last == current.next){
				last = current;
			}
			current.next = current.next.next;
			size--;
			return removed;
		}else{
			return removeHelper(current.next,item);
		}

	}

	public Iterator<T> iterator(){
		Iterator<T> iter = new Iterator<T>(){
			private int currentIndex = 0;
			private Node currentNode = null;
			public boolean hasNext() {
				if(currentIndex < size){
					return true;
				}else{
					return false;
				}
			}

			public T next() throws NoSuchElementException{
				if(currentNode == null){
					currentNode = first;
				}else{
					currentNode = currentNode.next;
				}
				
				currentIndex++;
				return currentNode.data;
			}

			
		};

		return iter;
	}

	public boolean isEmpty() {
		return size == 0;
	}
}
