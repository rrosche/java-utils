package list;


public class ListUtils {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static LinkedList mergeSort(LinkedList list){
		
		LinkedList.Node first = list.first;
		
		if(first == null || list.size == 1){
			return list;
		}
		
		LinkedList.Node newHead = mergeSort(list.first);
		LinkedList newList = new LinkedList(newHead);
		newList.size = list.size;
		return newList;
	}
	
	private static LinkedList<?>.Node mergeSort(LinkedList<?>.Node head){
		if(head == null || head.next == null){
			return head;
		}
		
		
		LinkedList<?>.Node middle = getMiddle(head);
		
		LinkedList<?>.Node rightOfMiddle = middle.next;
		middle.next = null;
		
		LinkedList<?>.Node left = mergeSort(head);
		LinkedList<?>.Node right = mergeSort(rightOfMiddle);
		
		return MergeSortedNodes(left,right);
	}

	private static LinkedList<?>.Node MergeSortedNodes(LinkedList<?>.Node left, LinkedList<?>.Node right) {
		
		if(left == null){
			return right;
		}else if(right == null){
			return left;
		}
		
		LinkedList<?>.Node result = null;

		if(left.compareTo(right) <= 0){
			result = left;
			result.next = MergeSortedNodes(left.next, right);
		}else{
			result = right;
			result.next = MergeSortedNodes(left, right.next);
		}
		
		return result;
	}

	private static LinkedList<?>.Node getMiddle(LinkedList<?>.Node head) {
		LinkedList<?>.Node slow;
		LinkedList<?>.Node fast;
		
		if(head == null || head.next == null){
			return head;
		}
		
		slow = head;
		fast = head.next;
		
		while(fast != null){
			fast = fast.next;
			if(fast != null){
				slow = slow.next;
				fast = fast.next;
			}
		}
		
		return slow;
	}
	
}
