package util;

public class Util {
	
	public static void print(Object object){
		System.out.println(object.toString());
	}
	
	public static String getMemoryUsage(){
		Runtime runtime = Runtime.getRuntime();
		
		runtime.gc();
		
		long memory = runtime.totalMemory() - runtime.freeMemory();
		
		return "Used Memory: " + memory/(1024L * 1024L) + " MB.";
	}
}
