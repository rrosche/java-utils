package tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trie {
	TrieNode root = new TrieNode((char) 0);
	
	protected class TrieNode{
		char character;
		Map<Character,TrieNode> children;
		boolean isWord;
		
		protected TrieNode(char character){
			this.character = character;
			children = new HashMap<>();
		}
	}
	
	public void insert(String word){
		word = word.toLowerCase();
		char[] stringArray = word.toCharArray();
		int length = stringArray.length;
		
		TrieNode currentNode = root;
		for(int i = 0; i < length; i++){
			TrieNode child = currentNode.children.get(stringArray[i]);
			if(child == null){
				child = new TrieNode(stringArray[i]);
				currentNode.children.put(child.character, child);
			}
			currentNode = child;
		}
		
		currentNode.isWord = true;
	}
	
	public List<String> complete(String incompleteWord){
		incompleteWord = incompleteWord.toLowerCase();
		List<String> words = new ArrayList<>();
		TrieNode searchStart = root;
		char[] incompleteWordArray = incompleteWord.toCharArray();
		String startingString = "";
		for(int i = 0; i < incompleteWordArray.length; i++){
			boolean matched = false;
			if(searchStart.children == null || searchStart.children.size() == 0){
				return words;
			}
			
			for(TrieNode node: searchStart.children.values()){
				if(node.character == incompleteWordArray[i]){
					matched = true;
					searchStart = node;
					if(i > 0){
						startingString += incompleteWordArray[i-1];
					}
				}
			}
			
			if(!matched){
				return words;
			}
		}
		
		findWords(searchStart,words, startingString);
		
		return words;
	}
	
	private void findWords(TrieNode node, List<String> words, String subString){
		if(node.isWord){
			words.add(subString + node.character);
		}
		if(node.children == null || node.children.size() == 0){
			return;
		}
		
		for(TrieNode childNode: node.children.values()){
			findWords(childNode,words,subString + node.character);
		}
	}
}
