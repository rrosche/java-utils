package tree;

public class AvlTree <T extends Comparable<T>>{
	Node root;
	int size;
	
	protected class Node implements Comparable<Node>{
		Node left;
		Node right;
		int height;
		T data;
		
		@Override
		public int compareTo(Node o) {
			return data.compareTo(o.data);
		}
		
		protected Node(T data){
			this.data = data;
		}
		
	}
	
	protected int height(Node node){
		return node==null?-1:node.height;
	}
	
	
	public void insert(T item){
		if(size == 0){
			root = new Node(item);
		}
		
		
	}
}
