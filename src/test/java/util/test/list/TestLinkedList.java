package util.test.list;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

import list.LinkedList;

public class TestLinkedList {
	
	@Test
	public void linkedListInsertOneTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		linkedList.insert("Test");
		
		Assert.assertTrue(linkedList.getFirst().equals("Test"));
		
		Assert.assertTrue(linkedList.getLast().equals("Test"));
		
		Assert.assertTrue(linkedList.size() == 1);
		
		
	}
	
	@Test
	public void linkedListLastTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		linkedList.insert("1");
		linkedList.insert("2");
		Assert.assertTrue(linkedList.getLast().equals("2"));
	}
	
	@Test
	public void linkedListFindTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		for(int i = 0; i < 1001; i++){
			linkedList.insert("Item_"+i);
		}
		
		Assert.assertTrue(linkedList.find("Item_800"));
		Assert.assertFalse(linkedList.find("Pizza"));
	}
	
	@Test
	public void linkedListIterTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		for(int i = 0; i < 1001; i++){
			linkedList.insert("Item_"+i);
		}
		
		Iterator<String> iter = linkedList.iterator();
		
		int count = 0;
		
		while(iter.hasNext()){
			Assert.assertTrue(iter.next().equals("Item_"+count++));
		}
	}
	
	@Test
	public void linkedListRemoveTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		for(int i = 0; i < 1001; i++){
			linkedList.insert("Item_"+i);
		}
		
		int size = linkedList.size();
		
		Assert.assertTrue(linkedList.remove("Item_300").equals("Item_300"));
		Assert.assertFalse(linkedList.find("Item_300"));
		Assert.assertTrue(linkedList.size() == (size-1));
	}
	
	@Test
	public void linkedListRemoveFirstTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		linkedList.insert("First");
		linkedList.insert("Second");
		linkedList.remove("First");
		
		Assert.assertTrue(linkedList.size() == 1);
		Assert.assertTrue(linkedList.getFirst().equals("Second"));
	}
	
	@Test
	public void linkedListRemoveEmpty(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		Assert.assertNull(linkedList.remove("Empty"));
	}
	
	@Test
	public void linkedListRemoveSingleList(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		linkedList.insert("First");
		linkedList.remove("First");
		
		Assert.assertTrue(linkedList.size() == 0);
		Assert.assertNull(linkedList.getFirst());
		Assert.assertNull(linkedList.getLast());
	}
	
	@Test
	public void linkedListRemoveLastTest(){
		LinkedList<String> linkedList = new LinkedList<>();
		
		linkedList.insert("First");
		linkedList.insert("Second");
		linkedList.remove("Second");
		
		Assert.assertTrue(linkedList.size() == 1);
		Assert.assertTrue(linkedList.getFirst().equals("First"));
		Assert.assertTrue(linkedList.getLast().equals("First"));
	}
	
}
